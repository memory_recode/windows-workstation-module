# PowerShell WindowsDeployment Module
The `WindowsDeployment` was built to help easily set up tools and configure settings after installing a fresh Windows OS.

## Requirements
* PowerShell Core 7.0+

## Installation
Download this repository and either copy the `WindowsDeployment` directory to one of your module locations, or import it directly. It's likely you will need to unblock the files unless you want to manually approve them every time you run a function.

### Module Directory
```
Unblock-File .\WindowsDeployment\*
Unblock-File .\WindowsDeployment\Public\*
Copy-Item -Path .\WindowsDeployment -Destination "$(($env:PSModulePath).split(';') | Select -Last 1)" -Recurse
Import-Module WindowsDeployment
```

### Direct Import
```
Unblock-File .\WindowsDeployment\*
Unblock-File .\WindowsDeployment\Public\*
Import-Module .\WindowsDeployment
```

---

## Available Functions
* [Set-WindowsTerminal](https://gitlab.com/memory_recode/windows-workstation-module/-/tree/main#set-windowsterminal)
* [Install-Applications](https://gitlab.com/memory_recode/windows-workstation-module/-/tree/main#install-applications)
* [Install-Fonts](https://gitlab.com/memory_recode/windows-workstation-module/-/tree/main#install-fonts)
* [Set-PowerShellProfile](https://gitlab.com/memory_recode/windows-workstation-module/-/tree/main#set-powershellprofile)
* [Set-WindowsSettings](https://gitlab.com/memory_recode/windows-workstation-module/-/tree/main#set-windowssettings)

---

## Set-WindowsTerminal
`Set-WindowsTerminal` is a cmdlet used for configuring the **Windows Terminal** application. The cmdlet modifies the *settings.json* configuration file loaded by **Windows Terminal**.
Using this function requires that Windows Terminal has been started at least once to generate base *settings.json*.

### Parameters
* `Setting`  
  String  
  The setting to modify. Example: `Profiles.defaults.font.face`  
  Setting should use a `.` (period) to separate child objects/json nodes

* `SettingValue`  
  String  
  The value to assign to Setting. Example: `"CaskaydiaCove Nerd Font"`

* `DefaultShell`  
  String  
  Sets default profile in Windows Terminal settings (should be the profile `Name`)

* `WorkDir`  
  String  
  Working directory for script to use for operations. Defaults to `$env:USERPROFILE\Downloads\pwshSetup`

### Example
Sets the default profile font face to **CaskaydiaCove Nerd Font** and default shell to **PowerShell**:  
`Set-WindowsTerminal -Setting "Profiles.defaults.font.face" -SettingValue "CaskaydiaCove Nerd Font" -DefaultShell 'PowerShell' -WorkDir "$($env:USERPROFILE)\Downloads\psWinSetup"`

---

## Install-Applications
`Install-Applications` is a cmdlet used for installing packages from **Winget**, **Microsoft App Store** (utilizes `Add-AppxPackage`), and downloading the **kubectl** and **terraform** binaries.

### Parameters
* `WingetPackages`  
  String(list)  
  List of Winget package IDs to install (equivalent to `winget install --id=${package_id}`)

* `AppStorePackages`  
  String(list)  
  List of Microsoft Store package names to install

* `Terraform`  
  Boolean  
  Set `$true` to download Terraform binary

* `TerraformVersion`  
  String  
  Version of Terraform to install. Options are a specific version (i.e. *1.1.2*) or *latest*

* `TerraformOutPath`  
  String  
  Path terraform binary downloads to

* `Kubectl`  
  Boolean  
  Set to `$true` to download Kubectl binary. Currently only supports latest version

* `KubectlOutPath`  
  String  
  Path kubectl binary downloads to

* `WorkDir`  
  String  
  Working directory for script to use for operations. Defaults to `$env:USERPROFILE\Downloads\pwshSetup`

### Example
Installs *Notepad++* and *Firefox* from **Winget**, *Windows Terminal* from **Microsoft App Store**, and downloads *Terraform v1.1.2* and *Kubectl latest* into *C:\tools*   
`Install-Applications -WingetPackages "Notepad++.Notepad++", "Mozilla.Firefox" --AppStorePackages "Microsoft.WindowsTerminal" -Terraform $true -TerraformVersion '1.1.2' -TerraformOutPath 'C:\tools' -Kubectl $true -KubectlOutPath 'C:\tools'`

---

## Install-Fonts
`Install-Fonts` is a cmdlet used for installing fonts.

### Parameters
* `FontUrl`  
  String  
  URL for function to download font archive (expects `.zip` archive only)

* `WorkDir`  
  String  
  Working directory for script to use for operations. Defaults to `$env:USERPROFILE\Downloads\pwshSetup`

### Example
Installs *Cascaydia Nerd Fonts*    
`Install-Fonts -FontUrl "https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/CascadiaCode.zip" -WorkDir "C:\temp\psWinSetup"`

---

## Set-PowerShellProfile
`Set-PowerShellProfile` is a cmdlet used to configure PowerShell profile settings.

### Parameters
* `PSProfile`  
  String  
  Path to the PowerShell profile to be updated

* `InstallOhMyPosh`  
  Boolean  
  `$true` will install *oh-my-posh* and dependencies. Requires compatible font for icons

* `AddAlias`  
  String  
  Adds specified alias, function, command to PowerShell profile

* `WorkDir`  
  String  
  Working directory for script to use for operations. Defaults to `$env:USERPROFILE\Downloads\pwshSetup`

### Example
Installs *oh-my-posh* and adds it to PowerShell profile. Adds alias `k` for *kubectl* command    
`Set-PowerShellProfile -PSProfile $PROFILE.CurrentUserAllHosts -InstallOhMyPosh $true -AddAlias 'Set-Alias k kubectl' -WorkDir "C:\temp\psWinSetup"`

---

## Set-WindowsSettings
`Set-WindowsSettings` is a cmdlet for customizing Windows settings.

### Parameters
* `AppTheme`  
  String  
  Accepts `Dark` or `Light` as parameters to set application theme

* `WindowsTheme`  
  String  
  Accepts `Dark` or `Light` as parameters to set system theme

* `EnableTaskbarSearch`  
  Boolean  
  `$false` disables Taskbar Search button. `$true` enables Taskbar Search button

### Example
Sets Application and System themes to **Dark** mode and disables *Taskbar Search* button    
`Set-WindowsSettings -AppTheme Dark -WindowsTheme Dark -EnableTaskbarSearch $false`

---