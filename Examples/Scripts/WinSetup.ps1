#Requires -RunAsAdministrator

Import-Module WindowsDeployment

$workDir = "$($env:USERPROFILE)\Downloads\WinSetup"
$wingetPackages = "Mozilla.Firefox", "Google.Chrome", "Discord.Discord", "7zip.7zip", "Dropbox.Dropbox", "Google.BackupAndSync"
$appStorePackages = "Microsoft.WindowsTerminal"
$aliases = '
$env:Path += ";C:\tools\"

Set-Alias k kubectl
Set-Alias tf terraform
function tfp {
  terraform plan $args
}
function tfa {
  terraform apply $args
}
function tfw {
  terraform workspace $args
}
'

Install-Fonts -FontUrl "https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/CascadiaCode.zip" -WorkDir $workDir
Install-Applications -WingetPackages $wingetPackages -AppStorePackages $appStorePackages -Terraform $true -TerraformVersion 'latest' -TerraformOutPath 'C:\tools' -Kubectl $true -KubectlOutPath 'C:\tools' -WorkDir $workDir
Set-PowerShellProfile -PSProfile $PROFILE.CurrentUserAllHosts -InstallOhMyPosh $true -AddAlias $aliases -WorkDir $workDir
Set-WindowsTerminal -Setting "Profiles.defaults.font.face" -SettingValue "CaskaydiaCove Nerd Font" -WorkDir $workDir
Set-WindowsTerminal -Setting "Profiles.defaults.useAcrylic" -SettingValue $true -WorkDir $workDir
Set-WindowsTerminal -Setting "Profiles.defaults.acrylicOpacity" -SettingValue 0.7 -WorkDir $workDir
Set-WindowsTerminal -DefaultShell 'PowerShell' -WorkDir $workDir
Set-WindowsSettings -AppTheme 'Dark' -WindowsTheme 'Dark' -EnableTaskbarSearch $false