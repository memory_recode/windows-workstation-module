#Requires -RunAsAdministrator
#Requires -Version 7.0
function Install-Fonts {
    <#
      .SYNOPSIS
      Install fonts system-wide (requires administrative privileges)

      .DESCRIPTION
      Install fonts system-wide (requiring administrative privileges). Uses $FontUrl to download a .zip archive,
      then extracts the archive and installs any .ttf file types to C:\Windows\Fonts

      .PARAMETER FontUrl
      URL where font archive exists

      .PARAMETER WorkDir
      Working directory for script to use for operations. Defaults to $env:USERPROFILE\Downloads\pwshSetup

      .EXAMPLE
      Install-Fonts -FontUrl "https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/CascadiaCode.zip" -WorkDir "C:\temp\psWinSetup"
      Installs CaskaydiaCove Nerd Fonts using C:\Users\User\Desktop\psWinSetup as the staging directory.
    #>
    [CmdletBinding()]Param(
        [Parameter(Position = 0, Mandatory = $false)][String]$FontUrl = "https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/CascadiaCode.zip",
        [Parameter(Position = 1, Mandatory = $false)][String]$WorkDir = "$($env:USERPROFILE)\Downloads\pwshSetup"
    )

    process {
        try
        {
            if (-not (Test-Path -Path $WorkDir)) {
                New-Item -ItemType Directory -Path $WorkDir
            }
            Write-Output "Downloading fonts..."
            Invoke-WebRequest -Uri $FontUrl -OutFile "$($WorkDir)\Fonts.zip"
            Write-Output "Extracting fonts..."
            Expand-Archive -Path "$($WorkDir)\Fonts.zip" -DestinationPath $WorkDir
            Unblock-File -Path "$($WorkDir)\*.ttf"
            Unblock-File -Path "$($WorkDir)\*.otf"
            Write-Output "Installing fonts..."

            $SystemFonts = "C:\Windows\Fonts"
            Get-ChildItem -Path $WorkDir\* -Include *.ttf -Recurse  | ForEach-Object {
                if (-not (Test-Path "$($SystemFonts)\$($_.Name)")) {
                    Copy-Item -Path $_.FullName -Destination $SystemFonts
                }
            }

            $fonts = Get-ChildItem -Path $WorkDir\* -Include *.ttf, *.otf -Recurse
            for ($x = 0; $x -le $fonts.count; $x++) {
                switch ($fonts[$x].Extension) {
                    '.ttf' {
                        if (-not (Get-ItemProperty -Path 'HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Fonts' -Name "$($fonts[$x].BaseName) (TrueType)" -ErrorAction SilentlyContinue)) {
                            New-ItemProperty -Name "$($fonts[$x].BaseName) (TrueType)" -Path "HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Fonts" -PropertyType string -Value $fonts[$x].Name
                        }
                        else {
                            Write-Output "Font $($fonts[$x].BaseName) already installed"
                        }
                    }
                    '.otf' {
                        if (-not (Get-ItemProperty -Path 'HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Fonts' -Name "$($fonts[$x].BaseName) (OpenType)" -ErrorAction SilentlyContinue)) {
                            New-ItemProperty -Name "$($fonts[$x].BaseName) (OpenType)" -Path "HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Fonts" -PropertyType string -Value $fonts[$x].Name
                        }
                        else {
                            Write-Output "Font $($fonts[$x].BaseName) already installed"
                        }
                    }
                }
            }

            Write-Output "Fonts successfully installed!"
            Write-Output "Cleaning up fonts from $WorkDir..."
            Remove-Item -Path "$($WorkDir)\*.ttf" -Force
            Remove-Item -Path "$($WorkDir)\*.otf" -Force
            Remove-Item -Path "$($WorkDir)\Fonts.zip" -Force
            return 0
        }
        catch
        {
            Write-Output $_.Exception.Message
            Write-Output "Install-Fonts returned an error"
        }
    }
}

Export-ModuleMember Install-Fonts