#Requires -RunAsAdministrator
function Set-WindowsSettings {
    <#
      .SYNOPSIS
      Configure Windows settings

      .DESCRIPTION
      Configure Windows settings (Dark/Light Theme for apps or system), configure
      Taskbar Search icon (disable/enable)

      .PARAMETER AppTheme
      Set application theme to Light or Dark

      .PARAMETER WindowsTheme
      Set Windows theme to Light or Dark

      .PARAMETER $EnableTaskbarSearch
      Enables or disables Taskbar Search

      .EXAMPLE
      Set-WindowsSettings -AppTheme Dark
      Sets Windows applications to use Dark theme

      .EXAMPLE
      Set-WindowsSettings -AppTheme Dark -EnableTaskbarSearch $false
      Sets Applications to use Dark theme and disables Taskbar Search

      .EXAMPLE
      Set-WindowsSettings -AppTheme Dark -WindowsTheme Dark -EnableTaskbarSearch $false
      Sets Windows and Applications to use Dark theme and disables Taskbar Search
    #>
    [CmdletBinding()]Param(
        [ValidateSet("Dark","Light")][Parameter(Position = 0, Mandatory = $true)][String[]]$AppTheme,
        [ValidateSet("Dark","Light")][Parameter(Position = 1, Mandatory = $true)][String[]]$WindowsTheme,
        [Parameter(Position = 2, Mandatory = $false)][Boolean]$EnableTaskbarSearch
    )

    process {
        try
        {
            if ($AppTheme) {
                switch ($AppTheme) {
                    'Dark' {
                        if ((Get-ItemProperty 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize' -Name AppsUseLightTheme -ErrorAction SilentlyContinue) -ne 0) {
                            if (-not (Get-ItemProperty 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize' -Name AppsUseLightTheme -ErrorAction SilentlyContinue)) {
                                New-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize' -Name AppsUseLightTheme -Value 0 -Type Dword
                            }
                            else {
                                Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize' -Name AppsUseLightTheme -Value 0 -Type Dword
                            }
                        }
                        else {
                            Write-Output "Application Dark theme already set."
                        }
                    }
                    'Light' {
                        if ((Get-ItemProperty 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize' -Name AppUseLightTheme -ErrorAction SilentlyContinue) -ne 1) {
                            if (-not (Get-ItemProperty 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize' -Name AppUseLightTheme -ErrorAction SilentlyContinue)) {
                                New-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize' -Name AppsUseLightTheme -Value 1 -Type Dword
                            }
                            else {
                                Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize' -Name AppsUseLightTheme -Value 1 -Type Dword
                            }
                        }
                        else {
                            Write-Output "Application Light theme already set."
                        }
                    }
                }
            }

            if ($WindowsTheme) {
                switch ($WindowsTheme) {
                    'Dark' {
                        if ((Get-ItemProperty 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize' -Name SystemUsesLightTheme -ErrorAction SilentlyContinue) -ne 0) {
                            if (-not (Get-ItemProperty 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize' -Name SystemUsesLightTheme -ErrorAction SilentlyContinue)) {
                                New-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize' -Name SystemUsesLightTheme -Value 0 -Type Dword
                            }
                            else {
                                Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize' -Name SystemUsesLightTheme -Value 0 -Type Dword
                            }
                        }
                        else {
                            Write-Output "Windows Dark theme already set."
                        }
                    }
                    'Light' {
                        if ((Get-ItemProperty 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize' -Name SystemUsesLightTheme -ErrorAction SilentlyContinue) -ne 1) {
                            if (-not (Get-ItemProperty 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize' -Name SystemUsesLightTheme -ErrorAction SilentlyContinue)) {
                                New-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize' -Name SystemUsesLightTheme -Value 1 -Type Dword
                            }
                            else {
                                Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize' -Name SystemUsesLightTheme -Value 1 -Type Dword
                            }
                        }
                        else {
                            Write-Output "Windows Light theme already set."
                        }
                    }
                }
            }

            switch ($EnableTaskbarSearch) {
                $true {
                    Write-Output "Enabling Taskbar Search box..."
                    Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Search' -Name 'SearchBoxTaskbarMode' -Value 1 -Force
                }
                $false {
                    Write-Output "Disabling Taskbar Search box..."
                    Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Search' -Name 'SearchBoxTaskbarMode' -Value 0 -Force
                }
                'default' {
                }
            }

            return 0
        }
        catch
        {
            Write-Output $_.Exception.Message
            Write-Host "Set-WindowsSettings returned an error"
        }
    }
}

Export-ModuleMember Set-WindowsSettings