# Function based on StackOverflow thread: https://stackoverflow.com/questions/48196089/add-nested-properties-to-a-powershell-object
function Add-NoteProperty {
    param(
        $InputObject,
        $Property,
        $Value
    )
    process {
        $path = $Property -split "\."
        $obj = $InputObject
        for ($x = 0; $x -lt $path.count -1; $x ++) {
            $propName = $path[$x]
            if (!($obj | Get-Member -MemberType NoteProperty -Name $propName)) {
                $obj | Add-Member NoteProperty -Name $propName -Value (New-Object PSCustomObject)
            }
            $obj = $obj.$propName
        }
        $propName = ($path | Select-Object -Last 1)
        if (!($obj | Get-Member -MemberType NoteProperty -Name $propName)) {
            $obj | Add-Member NoteProperty -Name $propName -Value $Value
        }
    }
}

function Set-WindowsTerminal {
    <#
      .SYNOPSIS
      Configure Windows Terminal app settings

      .DESCRIPTION
      Configures Windows Terminal app by modifying the JSON configuration

      .PARAMETER Setting
      The setting to modify. Example: Profiles.defaults.font.face

      .PARAMETER SettingValue
      The value to assign to Setting. Example: CaskaydiaCove Nerd Font

      .PARAMETER DefaultShell
      Sets default profile in Windows Terminal settings (selected by Name)

      .PARAMETER WorkDir
      Working directory for script to use for operations. Defaults to $env:USERPROFILE\Downloads\pwshSetup

      .EXAMPLE
      Set-WindowsTerminal -DefaultShell 'PowerShell'
      Sets the default profile to PowerShell

      .EXAMPLE
      Set-WindowsTerminal -Setting "Profiles.defaults.font.face" -SettingValue "CaskaydiaCove Nerd Font" -WorkDir "C:\temp\psWinSetup"
      Sets the default profile font to CaskaydiaCove Nerd Font

      .EXAMPLE
      Set-WindowsTerminal -Setting "Profiles.defaults.font.face" -SettingValue "CaskaydiaCove Nerd Font" -DefaultShell 'PowerShell' -WorkDir "C:\temp\psWinSetup"
      Sets the default profile font to CaskaydiaCove Nerd Font and the default profile to PowerShell
    #>
    [CmdletBinding()]Param(
        [Parameter(Position = 0, Mandatory = $false)][String]$Setting,
        [Parameter(Position = 1, Mandatory = $false)]$SettingValue,
        [Parameter(Position = 2, Mandatory = $false)][String]$DefaultShell,
        [Parameter(Position = 3, Mandatory = $false)][String]$WorkDir = "$($env:USERPROFILE)\Downloads\pwshSetup"
    )

    process {
        try
        {
            if (-not (Test-Path -Path $WorkDir)) {
                New-Item -ItemType Directory -Path $WorkDir
            }
            Write-Output "Checking for Windows Terminal settings.json..."
            $terminalSettings = Get-ChildItem "$($env:LOCALAPPDATA)\Packages\" | Where-Object {$_.Name -like 'Microsoft.WindowsTerminal*'}
            $settingsFile = "$($env:LOCALAPPDATA)\Packages\$($terminalSettings.Name)\LocalState\settings.json"
            if (Test-Path -Path $settingsFile) {

                if ($Setting -and $SettingValue) {
                    Write-Output "settings.json located at $settingsFile. Updating settings..."
                    $backupLoc = "$( $WorkDir )\winTerminalSettings.json.$( Get-Date -Format ddMMyyyyHHmm ).backup"
                    Copy-Item -Path $settingsFile -Destination $backupLoc
                    $settings = Get-Content -Path $settingsFile -Raw | ConvertFrom-Json
                    Add-NoteProperty -InputObject $settings -Property $Setting -Value $SettingValue
                    $settings | ConvertTo-Json -Depth 100  | Out-File $settingsFile -Force -Confirm:$false
                    Write-Output "Terminal Settings have been updated. Backup of original settings.json located at"
                    Write-Output $backupLoc
                }

                if ($DefaultShell) {
                    $checkProfile = ((Get-Content -Path $settingsFile -Raw) | ConvertFrom-Json).profiles.list | Where-Object {$_.Name -eq $DefaultShell}
                    $settings = Get-Content -Path $settingsFile -Raw | ConvertFrom-Json

                    if ($checkProfile) {
                        if ($settings.defaultProfile) {
                            Write-Output "Configuring default shell..."
                            $settings.defaultProfile = [String]"$($checkProfile.guid)"
                            $settings | ConvertTo-Json -Depth 100 | Out-File $settingsFile -Force -Confirm:$false
                            Write-Output "Default shell set to: "
                            Write-Output $profile
                        }
                        else {
                            $profileGuid = [String]"$($profile.guid)"
                            Add-NoteProperty -InputObject $settings -Property 'defaultProfile' -Value $profileGuid
                            $settings | ConvertTo-Json -Depth 100  | Out-File $settingsFile -Force -Confirm:$false
                            Write-Output "Default shell set to: "
                            Write-Output $profile
                        }
                    }
                    else {
                        $profiles = ((Get-Content -Path $settingsFile -Raw) | ConvertFrom-Json).profiles.list
                        throw "$DefaultShell not found in $settingsFile. Available profiles: `r`n$profiles"
                    }

                }

            }
            else {
                throw "Unable to find Windows Terminal settings. Please ensure the Windows Terminal application is installed and has been started."
            }
            return 0
        }
        catch
        {
            Write-Output $_.Exception.Message
            Write-Output "Set-WindowsTerminal returned an error"
        }
    }
}

Export-ModuleMember Set-WindowsTerminal