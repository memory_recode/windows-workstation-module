#Requires -RunAsAdministrator
function Install-Applications {
    <#
      .SYNOPSIS
      Install applications using Winget or Add-AppxPackage

      .DESCRIPTION
      Install list of packages (array) from the Windows store or Winget

      .PARAMETER WingetPackages
      List of Winget package IDs to install

      .PARAMETER AppStorePackages
      List of Microsoft Store package names to install

      .PARAMETER Terraform
      $true will download Terraform

      .PARAMETER TerraformVersion
      Version of Terraform to install

      .PARAMETER TerraformOutPath
      Path to download Terraform to

      .PARAMETER Kubectl
      $true will download Kubectl. Currently only supports latest

      .PARAMETER KubectlOutPath
      Path to download Kubectl to

      .PARAMETER WorkDir
      Working directory for script to use for operations. Defaults to $env:USERPROFILE\Downloads\pwshSetup

      .EXAMPLE
      Install-Applications -WingetPackages "7zip.7zip", "Microsoft.PowerShell" -AppStorePackages "Microsoft.WindowsTerminal"
      Installs 7zip and PowerShell Core from Winget, Windows Terminal from Microsoft Store

      .EXAMPLE
      Install-Applications -WingetPackages "Notepad++.Notepad++" --AppStorePackages "Microsoft.WindowsTerminal" -Terraform $true -TerraformVersion 'latest' -TerraformOutPath 'C:\tools' -Kubectl $true -KubectlOutPath 'C:\tools'
      Installs 7zip from Winget, Windows Terminal from App Store, and downloads latest version of Kubectl and Terraform to C:\tools

      .EXAMPLE
      Install-Applications -WingetPackages "Notepad++.Notepad++" --AppStorePackages "Microsoft.WindowsTerminal" -Terraform $true -TerraformVersion '1.1.2' -TerraformOutPath 'C:\tools' -Kubectl $true -KubectlOutPath 'C:\tools'
      Installs 7zip from Winget, Windows Terminal from App Store, and downloads Kubectl latest and Terraform 1.1.2 to C:\tools
    #>
    [CmdletBinding()]Param(
        [Parameter(Position = 0, Mandatory = $false)][String[]]$WingetPackages,
        [Parameter(Position = 1, Mandatory = $false)][String[]]$AppStorePackages,
        [Parameter(Position = 2, Mandatory = $false)][Boolean]$Terraform = $false,
        [Parameter(Position = 3, Mandatory = $false)][String[]]$TerraformVersion = 'latest',
        [Parameter(Position = 4, Mandatory = $false)][String[]]$TerraformOutPath = "$($env:USERPROFILE)\Downloads\pwshSetup",
        [Parameter(Position = 5, Mandatory = $false)][Boolean]$Kubectl = $false,
        [Parameter(Position = 7, Mandatory = $false)][String[]]$KubectlOutPath = "$($env:USERPROFILE)\Downloads\pwshSetup",
        [Parameter(Position = 8, Mandatory = $false)][String[]]$WorkDir = "$($env:USERPROFILE)\Downloads\pwshSetup"
    )

    process {
        try
        {
            if (-not (Test-Path -Path $WorkDir)) {
                New-Item -Path $WorkDir -ItemType Directory
            }

            if ($WingetPackages) {
                $WingetPackages | ForEach-Object {
                    $pkg = winget search --id=$_ --accept-source-agreements | Select-String -Pattern $_ -SimpleMatch
                    if ($pkg) {
                        $checkInstalled = winget list | Select-String -Pattern $_ -SimpleMatch
                        if (-not $checkInstalled) {
                            Write-Output "Installing package $_..."
                            winget install --id=$_ --silent --accept-package-agreements
                        }
                        else {
                            Write-Output "Package $_ already installed"
                        }
                    }
                    else {
                        Write-Output "Package ID $_ not found"
                    }
                }
            }
            if ($AppStorePackages) {
                $AppStorePackages | ForEach-Object {
                    $pkg = Get-AppxPackage $_
                    if ($pkg) {
                        if ($pkg.InstallLocation -notlike 'C:\*') {
                            Write-Output "Installing package $_..."
                            Add-AppxPackage $_ -Confirm:$false
                        }
                        else {
                            Write-Output "Package $_ is already installed"
                        }
                    }
                    else {
                        Write-Output "Package $_ not found"
                    }
                }
            }
            if ($Terraform) {
                $tfReleaseUrl = 'https://api.github.com/repos/hashicorp/terraform/releases'
                $tfReleases = Invoke-RestMethod -Method Get -Uri $tfReleaseUrl

                if (-not (Test-Path -Path $TerraformOutPath)) {
                    New-Item -Path $TerraformOutPath -ItemType Directory
                }

                if ($TerraformVersion -eq 'latest') {
                    $tfVersion = (($tfReleases) | Where-Object { $_.prerelease -ne 'true' })[0].Name.trim('v')
                    $tfArchive = "terraform_$($tfVersion)_windows_amd64.zip"
                    $tfDownloadUrl = "https://releases.hashicorp.com/terraform/$($tfVersion)/$($tfArchive)"
                    Invoke-WebRequest -Uri $tfDownloadUrl -OutFile "$($WorkDir)\$($tfArchive)" -ErrorAction SilentlyContinue

                    if (Test-Path -Path "$($WorkDir)\$($tfArchive)") {
                        Start-Sleep -Seconds 10
                        Unblock-File "$($WorkDir)\$($tfArchive)"
                        Expand-Archive -Path "$($WorkDir)\$($tfArchive)" -Destination "$TerraformOutPath"
                        Remove-Item -Path "$($WorkDir)\$($tfArchive)" -Force
                        Write-Output "Terraform downloaded to $($TerraformOutPath)\terraform.exe"
                    }
                    else {
                        throw 'Terraform download failed...'
                    }
                }
                else {
                    $tfVersion = (($tfReleases) | Where-Object { $_.name -match "^(v$TerraformVersion|$TerraformVersion)$" })[0].Name.trim('v')
                    $tfArchive = "terraform_$($tfVersion)_windows_amd64.zip"
                    $tfDownloadUrl = "https://releases.hashicorp.com/terraform/$($tfVersion)/$($tfArchive)"
                    Invoke-WebRequest -Uri $tfDownloadUrl -OutFile "$($WorkDir)\$($tfArchive)" -ErrorAction SilentlyContinue

                    if (Test-Path -Path "$($WorkDir)\$($tfArchive)") {
                        Start-Sleep -Seconds 10
                        Unblock-File "$($WorkDir)\$($tfArchive)"
                        Expand-Archive -Path "$($WorkDir)\$($tfArchive)" -DestinationPath "$TerraformOutPath" -Force
                        Remove-Item -Path "$($WorkDir)\$($tfArchive)" -Force
                        Write-Output "Terraform downloaded to $($TerraformOutPath)\terraform.exe"
                    }
                    else {
                        throw 'Terraform download failed...'
                    }
                }

            }
            if ($Kubectl) {
                $kubectlVersion = (Invoke-WebRequest -Method Get -Uri 'https://storage.googleapis.com/kubernetes-release/release/stable.txt').Content

                if (-not (Test-Path -Path $KubectlOutPath)) {
                    New-Item -Path $KubectlOutPath -ItemType Directory
                }

                $kubectlDownloadUrl = "https://dl.k8s.io/release/$($kubectlVersion)/bin/windows/amd64/kubectl.exe"
                Invoke-WebRequest -Uri $kubectlDownloadUrl -OutFile "$KubectlOutPath\kubectl.exe" -ErrorAction SilentlyContinue

                if (Test-Path -Path "$KubectlOutPath\kubectl.exe") {
                    Start-Sleep -Seconds 10
                    Write-Output "Downloading kubectl.exe..."
                    Unblock-File "$($KubectlOutPath)\kubectl.exe"
                    Write-Output "Kubectl downloaded to $($KubectlOutPath)\kubectl.exe"
                }
                else {
                    throw 'Kubectl download failed...'
                }
            }
            return 0
        }
        catch
        {
            Write-Output $_.Exception.Message
            Write-Host "Install-Applications returned an error"
        }
    }
}

Export-ModuleMember Install-Applications