function Set-PowerShellProfile {
    <#
      .SYNOPSIS
      Configure PowerShell Profile

      .DESCRIPTION
      Configures PowerShell Profile to add desired configurations

      .PARAMETER PSProfile
      The PowerShell profile to modify

      .PARAMETER InstallOhMyPosh
      Boolean. If true, installs oh-my-posh profile

      .PARAMETER AddAlias
      A string containing alias(es) to add to PowerShell profile

      .PARAMETER WorkDir
      Working directory for script to use for operations. Defaults to $env:USERPROFILE\Downloads\pwshSetup

      .EXAMPLE
      Set-PowerShellProfile -PSProfile C:\Users\User\Documents\PowerShell\profile.ps1 -InstallOhMyPosh $false -AddAlias 'k = kubectl' -WorkDir 'C:\Users\User\Desktop\psWinSetup'
      Adds alias for kubectl 'k' and uses C:\Users\User\Desktop\psWinSetup to store a backup of previous PowerShell profile.

      .EXAMPLE
      Set-PowerShellProfile -PSProfile C:\Users\User\Documents\PowerShell\profile.ps1 -InstallOhMyPosh $true -AddAlias 'k = kubectl' -WorkDir 'C:\Users\User\Desktop\psWinSetup'
      Installs oh-my-posh profile and adds alias for kubectl 'k'. Uses C:\Users\User\Desktop\psWinSetup to store a backup of previous PowerShell profile.
    #>
    [CmdletBinding()]Param(
        [Parameter(Position = 0, Mandatory = $true)][String]$PSProfile,
        [Parameter(Position = 1, Mandatory = $false)][Boolean]$InstallOhMyPosh = $true,
        [Parameter(Position = 2, Mandatory = $false)][String]$AddAlias,
        [Parameter(Position = 3, Mandatory = $false)][String]$WorkDir = "$($env:USERPROFILE)\Downloads\pwshSetup"
    )

    process {
        try
        {
            if ($InstallOhMyPosh) {
                Write-Output "Installing oh-my-posh..."
                Install-Module oh-my-posh -Scope CurrentUser -Confirm:$false
                Import-Module oh-my-posh
                Install-Module -Name Terminal-Icons -Repository PSGallery -Confirm:$false
            }
            if (-not (Test-Path $WorkDir)) {
                New-Item -ItemType Directory -Path $WorkDir
            }
            $backupLoc = "$($workDir)\psProfile.ps1.$(Get-Date -Format ddMMyyyyHHmm).backup"
            Write-Output "Updating $PSProfile. If a prior profile existed, a backup will be created at $backupLoc..."
            if (Test-Path -Path $PSProfile) {
                $checkProfilePosh = Select-String -Path $PSProfile -Pattern 'Set-PoshPrompt' -SimpleMatch
                $checkProfileIcons = Select-String -Path $PSProfile -Pattern 'Terminal-Icons' -SimpleMatch
                if ($null -eq $checkProfilePosh -and $null -eq $checkProfileIcons) {
                    Copy-Item -Path $PSProfile -Destination "$backupLoc"
                    '' | Out-File -FilePath $PSProfile -Append
                    'Set-PoshPrompt -Theme jandedobbeleer' | Out-File -FilePath $PSProfile -Append
                    'Import-Module -Name Terminal-Icons' | Out-File -FilePath $PSProfile -Append
                }
                elseif ($null -ne $checkProfilePosh -and $null -eq $checkProfileIcons) {
                    Copy-Item -Path $PSProfile -Destination $backupLoc
                    '' | Out-File -FilePath $PSProfile -Append
                    'Import-Module -Name Terminal-Icons' | Out-File -FilePath $PSProfile -Append
                }
                elseif ($null -eq $checkProfilePosh -and $null -ne $checkProfileIcons) {
                    Copy-Item -Path $PSProfile -Destination $backupLoc
                    '' | Out-File -FilePath $PSProfile -Append
                    'Set-PoshPrompt -Theme jandedobbeleer' | Out-File -FilePath $PSProfile -Append
                }
                else {
                    Write-Output "Profile located at $PSProfile already contains oh-my-posh configuration. Skipping..."
                }
            }
            else {
                Write-Output "Creating profile at $PSProfile..."
                New-Item -Path $PSProfile
                'Set-PoshPrompt -Theme jandedobbeleer' | Out-File -FilePath $PSProfile -Append
                'Import-Module -Name Terminal-Icons' | Out-File -FilePath $PSProfile -Append
            }

            $checkProfileAlias = Select-String -Path $PSProfile -Pattern $AddAlias -SimpleMatch
            if ($null -eq $checkProfileAlias) {
                '' | Out-File -FilePath $PSProfile -Append
                $AddAlias | Out-File -FilePath $PSProfile -Append
            }
            else {
                Write-Output "$AddAlias already exists in $PSProfile. Skipping..."
            }

            return 0
        }
        catch
        {
            Write-Output $_.Exception.Message
            Write-Output "Set-PowerShellProfile returned an error"
        }
    }
}

Export-ModuleMember Set-PowerShellProfile