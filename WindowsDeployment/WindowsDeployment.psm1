@{
    RootModule           = 'WindowsDeployment.psm1'
    ModuleVersion        = '1.0.0'
    CompatiblePSEditions = 'Core'
    Author               = ''
    CompanyName 		 = ''
    Copyright 		     = ''
    Description 		 = 'Windows Configuration Module'
    FunctionsToExport 	 = 'Set-WindowsTerminal','Install-Fonts','Install-Applications', 'Set-WindowsSettings', 'Set-PowerShellProfile'
    CmdletsToExport 	 = @()
    VariablesToExport 	 = @()
    AliasesToExport 	 = @()
    PrivateData 		 = @{}
    PSData 			     = @{}
}

Get-ChildItem (Split-Path $script:MyInvocation.MyCommand.Path) -Filter 'func_*.ps1' -Recurse | ForEach-Object {
    . $_.FullName
}
Get-ChildItem "$(Split-Path $script:MyInvocation.MyCommand.Path)\Public\*" -Filter 'func_*.ps1' -Recurse | ForEach-Object {
    Export-ModuleMember -Function ($_.BaseName -Split "_")[1]
}